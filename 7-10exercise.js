
// toString
const market = ["fruits", "vegitables", "sugar", "salt"];

const shoping = ["theater", "clothes", "shoes"] ;

const  arr1 = ["a", "b" , "c" , "d"];

console.log(market.toString()+"--tostringexample"); //tostring [Banana,Orange,Apple,Mango]

console.log(market.join("*") + "-- joinExample"); //join [Banana*Orange*Apple*Mango]

console.log(market.pop() + "-- popExample"); // pop mango [return the removed element its last elemt ]

console.log(market.push("bisuct") + "---pushExample"); // return the length of the arry with newly join elemtnt  

console.log(market.shift() + "--ShiftExample");  // remove the firtst elemt and change all element lower index  return the removed element

console.log(market.unshift("juse")+ "--unshiftExam");   // adds a new element to an begining array , return the newly added once

// concat ()
const overall = market.concat(shoping) // concat two array
console.log(overall + "--concatExam");
const ar = market.concat(shoping, arr1) // concaat three array 
console.log(ar);


// splice ()  , used to add new items to an arrayu. 
console.log(market)
console.log(market.splice(2,3, "haircutter" , "bulb"))
console.log(market + "---spliceExam")


//slice () , create new array, 
//selects array elements from the start argument, and up to (but not included) the end argument
const arr2 = ["aa", "bb" , "cc", "dd"]
console.log(arr2.slice(1,3) + "--sliceExam");


// sort() , arrange in alphabeticaly. 
const arr3 = ["z", "c" ,"b" , "a" , "g", "r"]
console.log(arr3.sort() + "--sortExam");

// reverse() , arragen in reverse alphabeticaly. 
console.log(arr3.reverse() + "--reverse");

// For numeric sort and reverse not working so we use some logical 
const num1 = [12 , 33, 43, 52, 23, 32, 66, 6 ];
num1.sort(function(a,b){return a -b }); // assending 
console.log(num1);
num1.sort(function(a,b){return b - a }); // decending
console.log(num1);

// foreach()
let num= "" ;
num1.forEach(fun);
function fun(value , index , array){ // value is mandatory 
    num +=value + "-";
}
console.log(num);
// or 
var x = [1,2,3,4,5,6];
x.forEach(a => x.push({name: a , age : a*2}))
console.log(x);

//map()
const numm = num1.map(fun2);
function fun2(value, index, array){
    return  value * 2;
}
console.log(numm +"---map");



//find return the fist passed value only
var num4 = [44, 55, 76, 45, 65, 23, 48, 81];
let result =  num4.find((value) => {
    return value > 30
});
console.log("o/p of find method-->" + result);

// filter 
var below8 = x.filter(a => a.age < 8) // its only return the value its passed true.
console.log(below8);


// some //any one of the value must be passed the conduction return true
let sum = num4.some((value) => {
    return value > 30
});
console.log(sum);

// every // all the element are passed the conduction  return true or false
let every = num4.every((value) => {
    return value > 10
});
console.log(every);

//include  // if the value are present on array its returns true. 
console.log(num4.includes(23)); 

